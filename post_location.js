//
// Requires:
//  @connect      emscoverage.com
//  @grant        GM_xmlhttpRequest
//  

function post_location(location) {

    var location_data = new FormData();
    location_data.append("location", location + '\n')

    console.log('Posting location: ' + location)

    var ret = GM_xmlhttpRequest({
        method: "POST",
        url: "http://emscoverage.com:8011/",
        data: location_data,
        onload: function(res) {
            console.log(res.responseText);
        }
    });

    console.log('got a response!!');
    console.log(ret)
}
